#!/bin/bash
#1. Ask for the package to be downloaded
#2. Then ask if it want to install from source code or with dpkg/rpm
#3. It then asks for the link to download the packages
#4. It checks/changes for the permission of the folder /usr/local/src such that everybody can
#      download and use packages downloaded there
#5. It then downloads the package in /usr/local/src
#6. It should then install the package depending on the choice of package downloaded.
#7. Report if the installation was successful
#8. If not then what was the reason (maybe there were some dependencies that were missing-
#      prompt to download and install those packages before downloaded the initial package that was
#      to be installed)
#9. find the package in apt-cache and prompt to install them and then reinstall the initial package to
#      be installed
#10. make it possible to run the script without sudo. Hint Look into sudoers file use visudo

# proposed script functionality:
# ask for package name
# ask for installation type (dpkg/rpm/source/git/other)
# ask for download link
# download to /usr/local/src (fix source)
# untar if necessary
# VERIFY download vs package name
# ANALYZE dependencies and build/make, notify user
# install package
# notify user
# error handling
# APT as emergency dependency fixer, then reinstall using this.script

# proposed generic functions:
# main menu that calls generic functions and handles errors, add [back_button] and [confirm_button] (generic function return codes)
# package analyzer that  (not required / not part of the scope of the assignment)
# file downloader that first checks if the file is already on disk (downloads folder or /usr/local/src)
# generic unpacker function
# dependency checker (not required / not part of the scope of the assignment)
# distinct install functions based on file type

# install from source:
# scan readme for dependencies, notify user
# scan source for actual package name (for path-related reasons)
# scan source for available installation scripts (./configure and variations)
# todo: if installation fails, notify user, auto-handling

fakeSelectionMenu () {
    # verified downloadable stuff
    gittarget='https://github.com/curl/curl.git'
    rpmtarget='https://mullvad.net/en/download/rpm/2020.3/'
    debtarget='https://mullvad.net/en/download/deb/2020.3/'
    bz2target='https://curl.haxx.se/download/curl-7.69.0.tar.bz2'
    ziptarget='https://curl.haxx.se/download/curl-7.69.0.zip'
    gztarget='https://curl.haxx.se/download/curl-7.69.0.tar.gz'
    xztarget='https://curl.haxx.se/download/curl-7.69.0.tar.xz'
    alientarget='http://archive.ubuntu.com/ubuntu/pool/universe/a/alien/alien_8.95_all.deb'

    # used for file operations on local files
#    packName='mkdev.sh' # can be edited to whatever
    installType='source'
#    sourceFile="~/Downloads/"
    packName='curl'
#    sourceFile="$xztarget"
#    sourceFile="$gztarget"
#    sourceFile="$bz2target"
    sourceFile="$bz2target"
    cleanSelectionMenu
}

# prepared strings - prompts to the user
prepareFixedStrings () {
    # general stuff
    result="Installation complete"
    badResult="Oops. Something went wrong"

    # testPermissions
    permissionsHelp="sudo chmod 777 /usr/local/src/"
    permissionsPassed="Permission check passed. Press any key to continue."
    permissionsFailed=

    # selectionMenu
    promptForPackage="Enter the name of your desired package"
    promptForType="Choose type of installation, (1) for source, (2) for package"
    promptForSource="Enter url or path to package. Leave blank if target in Downloads folder"
    printConclusion=
    promptForConfirmation="Confirm input to continue with installation (y), start over (n), exit (x)"
    notifySelectionSuccess="Selection complete. Press any key to continue"
    
    # downloadMenu
    promptForConclusion="Unable to determine source. Press (1) for local content, or (2) for remote"
    installationAborted="Installation aborted"
    validateFilename=
    validationPassed="Validation passed"
    fileNotFound=
    promptAnykey="Press any key to accept (ctrl+c to abort)"
    fileExists="File exists, skipping download"
    promptConfirmRemoteTarget=
    downloadSuccess="File succesfully downloaded"
    
    # unpackMenu
    quicktarSuccess="quick decompression succesful"
    quicktarFailed="quick decompression failed, going deeper..."
    notifyExtraction="Commencing extraction, this might take a while."
    notifyExtractionSuccess="Extraction succesful."
    
    # installationMenu
    beginConfiguration="Preparations complete, commencing configuration"
    beginMake="Configuration complete, commencing make"
    beginMakeInstall="Finished make, commencing make install"
}

promptContinue () {
    echo; echo "$1, press any key to continue."
    read anykey
}

updatePermissionsFailed () {
    permissionsFailed="You don't have permission to make changes to this folder, \\
                       your permision is $1 and it should be 777. You can change \\
                       the permission of the folder by using the following command:"
}

updateConclusion () {
    printConclusion="Source determined to be $conclusion. Analyzing..."
}

updateValidateFilename () {
    if [ "$conclusion" = "local" ]; then
        validateFilename="Validating local file $filepath$packName"
    else
        validateFilename="$1"
    fi
}

updateFileNotFound () {
    fileNotFound="Unable to locate $filepath$1"
}

updateConfirmRemoteTarget () {
    promptConfirmRemoteTarget="Confirm downloading $sourceFile to $dirtyTarget?"
}

# permission needed to install to /usr/local/src
testPermissions () {
access=$(stat -c "%a %n" /usr/local/src)
stringarray=($access)
permission=$(expr substr ${stringarray[0]} 2 2)
    if [ $permission != '77' ]; then
        updatePermissionsFailed "${stringarray[0]}"
        echo $permissionsFailed
        echo; echo $permissionsHelp
        result="$badResult during permissions testing."
        return 1
    else
        echo $permissionsPassed
        read anykey
        return 0
    fi
}

# echo function to display pre-installation selections
cleanSelectionMenu () {
    clear
    echo "This script will download and install whatever you tell it to"
    echo
    echo "Install folder: $packName"
    echo "Installation type: $installType"
    echo "Source of installation media: $sourceFile"
    echo
}

cleanDownloadMenu () {
    cleanSelectionMenu
    echo $printConclusion
    echo "$validateFilename"
    echo "$validationPassed"
}

cleanUnpackMenu () {
    cleanDownloadMenu
}

cleanInstallationMenu () {
    cleanUnpackMenu
}

# menu function - lets the user select package, installation type and url/path
# TODO - validate package name with apt-cache or similar, notify user if not in repository
# TODO - validate url/path - for now, trust the user
# TODO - if url/path is blank, set to $HOME/Downloads
# TODO - verify that 
selectionMenu () {
    # package name
    cleanSelectionMenu
    echo $promptForPackage
    read packName

    # installation type, source or package
    cleanSelectionMenu
    echo $promptForType
    # validate user input - infinite loop until validation succeeds
    while true
    do
        local input=$(userInput)
        if [ "$input" = '1' ] || [ "$input" = '2' ]; then
            break
        fi
    done
    # response to user input - only 1 and 2 allowed
    case $input in
        '1')
            installType='source'
            ;;
        '2')
            installType='package'
            ;;
    esac

    # url/path, blank for Downloads folder
    cleanSelectionMenu
    echo $promptForSource
    read sourceFile
    if [ "$sourceFile" = "" ]; then
        sourceFile="$HOME/Downloads/"
    fi

    # confirmation before installing selection
    cleanSelectionMenu
    echo $promptForConfirmation
    # validate user input - infinite loop until validation succeeds
    while true
    do
        local input=$(userInput)
        if [ "$input" = 'y' ] || [ "$input" = 'n' ]; then
            break
        elif [ "$input" = 'x' ]; then
            echo "$result"
            exit 0
        fi
    done
    # response to user input - y to continue with selected parameters, n to start over
    case $input in
        'y')
            cleanSelectionMenu
            echo "$notifySelectionSuccess"
            read anykey
            return 0
            ;;
        'n')
            packName=""
            installType=""
            sourceFile=""
            selectionMenu # recursive call to self
            return 0
            ;;
    esac
}

# helper function to capture user input
userInput () {
    read input
    echo $input
}

downloadMenu () {
    # test if local file or download
    length=${#sourceFile}
    if [ ${sourceFile:0:1} = "~" ]; then
        # local - based in home dir - convert to true path
        conclusion='local'
        sourceFile="$HOME${sourceFile:1:$length-1}"
        length=${#sourceFile} # update length
    elif [ ${sourceFile:0:1} = "/" ]; then
        # local filepath - skip download
        conclusion='local'
    elif [ ${sourceFile:0:4} = 'http' ]; then
        # remote - download needed
        conclusion='remote'
    else
        # prompt user for type
        echo $promptForConclusion
        while true
        do
            local input=$(userInput)
            if [ "$input" = '1' ]; then
                conclusion='local'
                break
            elif [ "$input" = '2' ]; then
                conclusion='remote'
                break
            fi
        done
    fi    
    updateConclusion "$conclusion"
    echo $printConclusion
    # if local file - go do that instead of downloading stuff
    if [ "$conclusion" = 'local' ]; then
        localPreparations
        return 0
    fi    
    # if remote file - commence downloading
    if [ "$conclusion" = 'remote' ]; then
        downloadFile
        return 0
    fi
    result="$badResult, unable to verify source file."
    return 1
}

# remaining local file operations before installing - migrated to reduce bloating
localPreparations () {
    filepath=
    filename=
    i=0
    # split actual filename from the rest, for reasons...
    while [ $i -lt $length ]; do
        char=${sourceFile:$i:1}
        if [ $char = "/" ]; then
            filepath=${sourceFile:0:($i+1)}
            filename=${sourceFile:($i+1):$length}
        fi
        i=$(expr $i + 1)
    done
    # check if [package_name] is present in the target directory
    # TODO error handling - prompt for selectionmenu or exit
    # TODO - autocomplete incomplete package names - e.g correct 'Mullvad' to 'MullvadVPN-2020.3_amd64.deb'
    # TODO - autocorrect poor spelling - e.g correct 'MullvadVPN-2020.3_x86_64.rpm' to MullvadVPN-2020.3_x86_64.rpm
    if [ $filename='' ]; then
        updateValidateFilename
        echo "$validateFilename"
        if [ ! -f $filepath$packName ]; then
            updateFileNotFound "$packname"
            echo "$fileNotFound"; echo "$installationAborted"
            result="$badResult, local file could not be verified."
            return 1
        else
            filename="$packName"
            echo "$validationPassed"
            return 0
        fi
    elif [ ! -f $filepath$fileName ]; then
        updateFileNotFound "$filename"
        echo "$fileNotFound"; echo "$installationAborted"
        result="$badResult, local file could not be verified."
        return 1
    else
        echo "$validationPassed"
        return 0
    fi
}

# self-explanatory - migrated to reduce bloating
downloadFile () {
    # late comment - variable named while doing a quick'n'dirty workaround
    # now it's just too late to refactor, but at least it's honest
    dirtyTarget="/usr/local/src/$packName/"
    i=0
    length=${#sourceFile}
    # once again extracting filename from the rest
    while [ $i -lt $length ]; do
        char=${sourceFile:$i:1}
        if [ $char = "/" ]; then
            urlprefix=${sourceFile:0:($i+1)}
            filename=${sourceFile:($i+1):$length}
        fi
        i=$(expr $i + 1)
    done
    echo "filename: $filename"

    # test if file exists - no need to download something we already have
    if [ -f $dirtyTarget$filename ]; then
        echo "$fileExists"
        return 0
    fi

#    wget -nv # no-verbose # not quite quiet, displays error msgs and basic info    
#    wget --spider # no download # verifies target existence
    remoteTarget=$(wget -nv --spider $sourceFile)

    updateValidateFilename "$remoteTarget"
    updateConfirmRemoteTarget
    echo "$promptConfirmRemoteTarget"
    echo "$promptAnykey"
    read anykey

#    wget -nv # no-verbose # not quite quiet, displays error msgs and basic info
#    wget -nc # no clobber # cancel download if filename exists in target dir # MANDATORY
#    wget --no-check-certificate # ignore certificate warnings
#    wget --show-progress # self-explanatory # nice
#    wget -P # directory prefix # where to save stuff # default is current working dir
    wget -nv -nc --no-check-certificate --show-progress "$sourceFile" -P "$dirtyTarget"

    # test if file was actually downloaded
    # should probably look up a return code from wget above, since an incomplete dl would trigger this
    if [ -f $dirtyTarget$filename ]; then
        echo "$downloadSuccess"
        return 0
    fi
    result="$badResult while downloading."
    return 1
}

# TODO - zip stuff. it works, but just barely.
# TODO - more output to user, some might think the scipt halted
unpackMenu () {
    file -b "$dirtyTarget$filename"
    if [ "$installType" = 'package' ]; then
        return 0
    fi
    # determine compression type
    compressionType=$(file -b "$dirtyTarget$filename" | cut -d ' ' -f 1)
    # logfile needed for installation
    # could probably do a workaround for this and pipe output to /dev/null instead
    logfile="$dirtyTarget$packName.LOG"
    touch "$logfile"
    extractionSuccess=
    tarfile=
    echo "$notifyExtraction"
    
    # switch, lots of different archive formats, and potential for undiscovered cases
    # TODO - zip-related stuff. lots of it.
    case "$compressionType" in
        'Zip') # probably more formats to catch
            unzip -an "$dirtyTarget$filename" -d "$dirtyTarget"
            extractionSuccess=true
            tarfile=false
            ;;
        'bzip2' | 'gzip' | 'XZ' )
            tar -xvf "$dirtyTarget$filename" -C "$dirtyTarget" --skip-old-files --force-local --warning=none > "$logfile" 2>&1
            extractionSuccess=true
            tarfile=true
            ;;
        '*' )   # catch-all for tar types
            tar -xvf "$dirtyTarget$filename" -C "$dirtyTarget" --skip-old-files --force-local --warning=none > "$logfile" 2>&1 
            extractionSuccess=true
            ;;
    esac
    if [ $extractionSuccess ]; then
        echo "$notifyExtractionSuccess"
        return 0
    else
        result="$badResult during extraction"
        echo $result
        return 1
    fi
}

# TODO - more output to user, some might think the scipt halted
installationMenu () {
    echo "$installType"
    if [ $installType = 'source' ]; then
        echo "source stuff"
        sourceStuff
    elif [ $installType = 'package' ]; then
        echo "package stuff"
        packStuff
    else
        # exit
        exit
    fi
}

packStuff () {
#    downloadlink = $1
#    packageName = $2
#    echo $packageName
#    echo $downloadLink

#    wget -O /usr/local/src/$packageName $downloadLink$packageName
    packageSize=${#filename}
    #echo $(expr substr $filename $((packageSize-2)) 3)
    cd /usr/local/src/
    #echo $filename
    if [ $(expr substr $filename $((packageSize-2)) 3) == 'rpm' ]
    then 
        echo this is an RPM file.
        alien -k $dirtyTarget$filename
        i=$(alien -k $dirtyTarget$filename)
        echo "alien"
        i=0
        while :; do
            echo "."
            wait 1
            i=$(expr $i + 1)
        done
        itemp=( $i )
        echo "remove"
#        rm $filename
        filename=${itemp[0]}
        echo $filename
    fi
   
    dpkg -i $dirtyTarget$filename  #&> /dev/null
#    echo "dpkg"
    if [ $? -ne 0 ]
        then
            echo "There was a problem with the installation, probably missing dependencies"
    ##PSEUDO CODE FOR HANDLING MISSING DEPENDENCIES
    # dpkgResult = dpkg -i $packageName
    # If (dpkgResult.contains "dependency problems") {
    #    loop {
    #        dependency[counter] = parse dpkgResult for missing dependencies
    #       } (Until no more missing dependencies)
    #    loop {
    #        download dependency[counter]
    #        install dependency[counter]
    #       }
    #
            result="Package did not install correctly."
        else    
            result='Package Installed Correctly'
    fi
}
#    apt-cache search "$filename"
#    apt-cache depends "$filename"
#    apt-cache unmet "$filename"
sourceStuff () {
    # the very first line in the archive is *usually* a directory containing everything else
    # TODO - fallback if this is not the case, as it will break everything atm
    installDir=$(head -n 1 "$logfile")
    cd "$dirtyTarget$installDir"
    currentdir=$(pwd)

    # testing for ./configure
    # TODO - fallback method if no configure
    if ( ls configur* > /dev/null ); then
        promptContinue "$beginConfiguration"
        # throw all the data at the user - not perfect
        ./configure --prefix="$dirtyTarget"
        # the alternative, no output - not perfect either
#        ./configure --prefix="$dirtyTarget" | tee >> "$logfile"

        promptContinue "$beginMake"
        # same as before, throw output at user
        make
        # while this little piggy didn't kiss and tell
#        make | tee >> "$logfile"

        promptContinue "$beginMakeInstall"
        # user output
        make install
        # no user output
#        make install | tee >> "$logfile"

        # sort of fix for lack of admin privileges
        export PATH="$dirtyTarget'bin':$PATH"
        return 0
    fi
    result="$badResult, no configuration file found"
    return 1
}


# deprecated
sourceDownload() {
    packageName=$1
    downloadlink=$2
    downloaddir=$3
    targetdir=$4
    filename=$5

    echo "choose source"
    echo "1) git clone"
    echo "2) tar.bz2"
    read conf
    case $conf in # git
    "1")
        cd $targetdir
#        workingDirectory=$(pwd)
#        echo "working directory: $workingDirectory"
#        echo "install $packageName to $workingDirectory (y/n)?"
#        read conf
#        if [ "$conf" = y ]
#        then
        echo "gitcloning stuff"
#        git clone $downloadlink
#        fi
        ;;
    "2") # tar.bz2
        if ls $downloaddir | grep $packageName
        then
            echo "unpacking stuff"
#            return 1
#            wget -O $downloaddir$filename $downloadlink
        fi
        cd $targetdir
        workingDirectory=$(pwd)
        echo "unpacking files to $workingDirectory"
#        tar xf "$downloaddir""$filename"
#        return 0
    esac
#    doSource "$targetdir"
#    dpkgSourceStuff "$packageName" "$downloadlink"
}

# might be useful
sourceFromGit () {
    downloadlink=$1
    targetdir=$2
    packageName=$3
    cd $targetdir
    workingDirectory=$(pwd)
    echo "working directory: $workingDirectory"
    echo "install $packageName to $workingDirectory (y/n)?"
    read conf
    if [ "$conf" = y ]
    then
        git clone $downloadlink
    fi
}

# deprecated
doSource () {
    targetdir=$1
    # check dependencies
    # build dependencies (apt-get build-dep package) note: message to user
    cd "$targetdir""$packageName"/
    ./configure --prefix="$targetdir""$packageName"/
    make
    make install
}

# freed to mitigate bloating - will be merged back later
dpkgRpmDownload() {
    result='RPM is gut'
}

# actual script
prepareFixedStrings
testPermissions
selectionMenu
#fakeSelectionMenu
downloadMenu
unpackMenu
installationMenu
echo "$result"
exit 0
# script end
echo "panic, break out manually!"
read input


# OLD STUFF - replaced

# fixed test params
downloaddir=~/Downloads/
#packageName='GTI_TarInstall'
#downloadlink='https://gitlab.com/62501_t7g1/gti_tarinstall.git'
targetdir='/usr/local/src/'
