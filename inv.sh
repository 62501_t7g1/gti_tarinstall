#!/bin/bash
package=$1
all_dep=$(apt-cache depends "$package")
inst_dep=$(apt-cache depends --installed "$package")


getRelease () {
    apt indextargets | grep -e Codename -e Origin -e Version
    cat /var/lib/dpkg/available
}



declare -a allDepends=
alldep=($(echo ${all_dep//\: / } | tr -s " "))
i=0
for dep in ${alldep[@]}; do
    if [ "$dep" = "Depends" ]; then
        continue
    elif [ "$dep" = "$package" ]; then
        continue
    fi
    allDepends[$i]=$dep
    (( i++ ))
done

declare -a installedDepends=
instdep=($(echo ${inst_dep//\: / } | tr -s " "))
i=0
for dep in ${instdep[@]}; do
    if [ "$dep" = "Depends" ]; then
        continue
    elif [ "$dep" = "$package" ]; then
        continue
    fi
    installedDepends[$i]=$dep
    (( i++ ))
done

declare -a missingDepends=
i=0
for dep in ${allDepends[@]}; do
    echo "testing $dep"
    installed=false
    for idep in ${installedDepends[@]}; do
        if [ "$idep" = "$dep" ]; then
            installed=true
#            echo "$dep already installed"
        fi
    done
    if ! $installed; then
        missingDepends[$i]=$dep
    fi
    (( i++ ))
done


echo; echo "missing dependencies:"
for dep in ${missingDepends[@]}; do
    echo "$dep"
#    echo $dep | apt indextargets
done

echo; echo "missing, xargs:"
for dep in $missingDepends; do
    echo "$dep"
done

echo; echo "depends list complete"; echo

# stderr=
# echo "redirect stderr - press anykey to continue"
# read anykey
# stdout=$(apt-cache showsrc "$package" 2> /dev/null)
# echo "redirect stdout - press anykey to continue"
# read anykey
# stderr=$(apt-cache showsrc "$package" 1> /dev/null)
setout () {
    stdout="$1"
}
seterr () {
    stderr="$1"
}

if [ ! -d /tmp/gti2/ ]; then
    mkdir /tmp/gti2
fi
> /tmp/gti2/gti2std.out
> /tmp/gti2/gti2std.err
cd /tmp/gti2/

# apt-cache show "$package" 2>gti2std.err 1>gti2std.out
dpkg -s "$package" 2>gti2std.err 1>gti2std.out

cat gti2std.out

cat gti2std.err




# echo "apt build-dep --dry-run $package"
# apt build-dep --dry-run $package
# buildDep=$(apt build-dep --dry-run $package | grep -A 2 NEW)
# echo "$buildDep"

# --no-list-cleanup = ignore updating /var/lib/apt/lists
# --download-only = package files not unpacked or installed
# apt build-dep --dry-run --download-only
# echo "using listWithIFS function"
# listWithIFS "$all_dep" '\n'

# echo "using listWithBash function"
# listWithBash "$all_dep"

# echo '$inst_dep' # prints literal $all_dep
# echo $inst_dep   # prints output in single space-separated line
# echo "$inst_dep" # prints output in single space-separated line (xargs)

# listAllDepends
# listInstalledDepends

# declare -a files=($all_dep,$inst_dep)
# echo "missing depends"


# diff "$all_dep" "$inst_dep"
# diff -a "$files"
# diff -y "$files"

# comm $(expr strip "$all_dep") $(expr strip "$inst_dep")

#installed=
#for dep in "${all_dep[@]}"; do
#    for inst in "${inst_dep[@]}"; do
#        if [ "$dep" = "$inst" ]; then
#            echo "match: $dep already installed"
#            installed=true
#        else
#            echo "$dep - $inst"
#        fi
#    done
#    if $installed; then :; else
#        echo "$dep"
#        installed=false
#    fi
#done


#while IFS=';' read -ra ADDR; do
#    for i in "${ADDR[@]}"; do
#        # process "$i"
#    done
#done <<< "$IN"

exit
