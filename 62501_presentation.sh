#!/bin/bash

present_start="presentation starting, say something smart"
topic_root_tree="root@root:$ tree -dL 1"
topic_boot_ls="root@root:$ ls /boot/"
topic_dev_ls="root@root:$ ls /dev/"
topic_etc_tree="root@root:$ tree -dL 4 /etc/ | less"
topic_media_ls="root@root:$ ls /media/"
topic_mnt_tree="root@root:$ tree -d /mnt/"
topic_opt_tree="root@root:$ tree -d /opt/ | less"
topic_run_ls="root@root:$ ls /run/"
topic_tmp_ls="root@root:$ ls /tmp/"
topic_var_tree="root@root:$ tree -dL 2 /var/ | less"
topic_usr_tree="root@root:$ tree -dL 2 /usr/ | less"
topic_usr_bin_ls="root@root:$ ls /usr/bin/ | less"
topic_usr_lib_tree="root@root:$ tree -d /usr/lib/ | less"
topic_usr_local_tree="root@root:$ tree /usr/local/ | less"
topic_usr_sbin_ls="root@root:$ ls /usr/sbin/ | less"
topic_usr_share_tree_long="root@root:$ tree -d /usr/share/ | less"
topic_usr_share_tree_short="root@root:$ tree -dL 2 /usr/share/ | less"
topic_usr_src_tree="root@root:$ tree -dL 2 /usr/src/ | less"
present_end="presentation finished, exiting"
press_anykey="press any key to continue"

clear
cd /
echo; echo "$present_start"

echo; echo "$topic_root_tree"
read -sn 1 anykey
tree -dL 1
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_boot_ls"; echo
read -sn 1 anykey
ls /boot/
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_dev_ls"; echo
read -sn 1 anykey
ls /dev/
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_etc_tree"; echo
read -sn 1 anykey
tree -dL 4 /etc/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_media_ls"; echo
read -sn 1 anykey
# ls /media/
echo "  backup"
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_media_ls""backup/"; echo
read -sn 1 anykey
echo "  all_music_ever_made"
echo "  arbitrary_folder_name"
echo "  lawsuits_waiting_to_happen"
echo "  movies_with_keanu"
echo "  other_movies"
echo "  sent_dick_picks_cloud_backup"
echo "  worlds_largest_porn_collection"
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_mnt_tree"; echo
read -sn 1 anykey
tree -d /mnt/
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_opt_tree"; echo
read -sn 1 anykey
tree -d /opt/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_run_ls"; echo
read -sn 1 anykey
ls /run/
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_tmp_ls"; echo
read -sn 1 anykey
ls /tmp/
echo; read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_var_tree"; echo
read -sn 1 anykey
tree -dL 2 /var/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_tree"; echo
read -sn 1 anykey
tree -dL 2 /usr/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_bin_ls"; echo
read -sn 1 anykey
ls /usr/bin/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_lib_tree"; echo
read -sn 1 anykey
tree -d /usr/lib/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_local_tree"; echo
read -sn 1 anykey
tree /usr/local/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_sbin_ls"; echo
read -sn 1 anykey
ls /usr/sbin/ | less
read -p "$press_anykey" -sn 1 anykey

# /usr/share is for 
echo; echo; echo "$topic_usr_share_tree_long"; echo
read -sn 1 anykey
tree -d /usr/share/ | less # bigass tree struct
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_share_tree_short"; echo
read -sn 1 anykey
tree -dL 2 /usr/share/ | less # still big
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$topic_usr_src_tree"; echo
read -sn 1 anykey
tree -dL 2 /usr/src/ | less
read -p "$press_anykey" -sn 1 anykey

echo; echo; echo "$present_end"; echo
read -sn 1 anykey
exit 0
