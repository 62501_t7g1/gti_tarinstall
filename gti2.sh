#!/bin/bash
#
# TODO - license - beerware, gpl, other? beerware works for now
#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file.  As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.
#
# For now, the beerware license is in effect, but the I reserve the right to
# change it to any non-copyright license. I still ponder about whether strict
# copy-left licenses are too intrusive. I just want people to use my work if
# it's the right tool for the job, and I don't care if you make money using it.
# just buy me a beer some time if:
# a) we coincidentally run into each other at the airport;
# b) you feel like you can afford it, and
# c) you feel like I earned it.
#
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------
#
# Desciption:
#
# This script will install pretty much anything, even without root privileges.
# This script is not intended to replace APT, YUM, pacman or whatever, but
# there are usecases that justify my time spent, e.g on a workstation without
# root privileges, or when troubleshooting the laptops of tech-illiterate 
# acquaintances. In either case, you are not forced to use or condone this 
# effort, and it is improbable that I would lose sleep if you don't.
#
# Use at your own risk. It should be safe without sudo, but testing was limited
# to Ubuntu 19.10 Eoan and whatever tools was preinstalled.
#
# ----------------------------------------------------------------------------
#
GTI2_NAME="gti2"
GTI2_LONG_NAME="GTI Tar Install"
GTI2_VERSION="1.0"
GTI2_INST_DIR_IF_ROOT="/usr/src/$GTI2_NAME"
GTI2_INST_DIR_IF_PREF="/usr/local/src/$GTI2_NAME"
GTI2_INST_DIR_IF_HOME="$HOME/.local/share/$GTI2_NAME"
#
# ----------------------------------------------------------------------------
#
# TODO - better name - choose whatever from the following list
# P (poly, as in many/multiple)
# G (generic, as in the opposite of specific)
# U (universal, as in works in all distros)
# S (source-independent, as in accepting any user input)
# R (root-less, as in functional without root privileges) rootful? maybe to modern
# I (installer, as in functionality)
#
# maybe GSIRI Source-Independent Rootless Installer - too long? too hard to pronounce?
# reminder: ensure name is not already taken
#
# TODO - notes
# auto-install dependencies functionality
#   current headaches:
#       overwriting state/progress for current package - fixed
#       determining link to depends (uname -v for installed distro) - in progress
#   known repositories: research all the other distros
#       archive.ubuntu.org/ubuntu/pool/* - workaround with apt/sources.list - not perfect
#       ftp.debian.org/debian/pool/*     - might need to hardcode shit
#       ftp.altlinux.org/pub/
#   possible solution:
#       implementing commandline arguments to quickset selection - done
#       store original and depends in objects, then call necessary menus - done
#       expanded headaches - must implement --ignore-deps and --assume-yes - in progress

# TODO - the big list - remember to update
# new functionality - database for previous installations with this.script
# dynamic strings (general) - streamline the way dynamic strings work, it's a mess right now
# dynamic strings (general) - streamline userprompts (read anykey)
# cleanUI (general) - cleanup userinput - reflection: echo verbose actions, and clean afterwards
# installation procedure - source from git
# variables (general) - strings: rename to updateXX, promptXX (read), notifyXX, warnXX (read)
# TODO variables (general) - track variables throughout, which are used when and where, document at beginning of function
# quickstart debugging - remove when appropriate
# parseArgs() - implement commandline arguments to quickset selections
# parseArgs() - new function that ONLY exports path of previous installations           (enable all)
# parseArgs() - new function that checks previous installations for possible updates    (update all)
# prepareFixedStrings() - ensure all simple output strings are migrated to this function
# updateConfirmRemoteTarget() - rename to promptConfirmTarget and snag the read() anykey
# testPermissions() - determine if root privileges and do proper installs if possible
# testPermissions() - get and store system specs and capability for later use - e.g RPM disabled
# localPreparations() - autocomplete incomplete package names - e.g correct 'Mullvad' to 'MullvadVPN-2020.3_amd64.deb'
# localPreparations() - autocorrect poor spelling - e.g correct 'MullvadVPN-2020.3_x86_64.rpm' to MullvadVPN-2020.3_x86_64.rpm
# handleArchive() - output control - don't echo 3k lines of files, but echo something
# handleArchive() - simplification - let's just test the compression and support only specific formats
# handleArchive() - functionality  - support more formats
# handleInstallation() - reflection - this is too simple, is it even needed?
# validateDepends() - temporary file handling - what and when to delete
# validateDepends() - depends info - make sure it works for all file types incl source
# validateDepends() - feature - auto-install dependencies
# installPackage() - optimize for .deb files using dpkg
# installPackage() - implement solutions for non-deb packages (debian/ubuntu needs sudo to gain rpm functionality)
# installSource() - fallback if this is not the case, as it will break everything atm
# installSource() - fallback method if no configure
# installSource() - proper export path - verify actual dirs needed


# debug function for skipping the interactive selection menu
quickDebugSelection () {
    quickStart=false


    # remote targets
#    doRemoteCurlBZ2 () {
#        packageName='curl'
#        remoteURL='https://curl.haxx.se/download/curl-7.69.0.tar.bz2'
#    }

#    doRemoteCurlZIP () {
#        packageName='curl'
#        remoteURL='https://curl.haxx.se/download/curl-7.69.0.zip'
#    }

#    doRemoteCurlGZ () {
#        packageName='curl'
#        remoteURL='https://curl.haxx.se/download/curl-7.69.0.tar.gz'
#    }

#    doRemoteCurlXZ () {
#        packageName='curl'
#        remoteURL='https://curl.haxx.se/download/curl-7.69.0.tar.xz'
#    }

#    doRemoteMullvadRPM () {
#        packageName='mullvad'
#        remoteURL='https://mullvad.net/en/download/rpm/2020.3/'
#    }

#    doRemoteMullvadDEB () {
#        packageName='mullvad'
#        remoteURL='https://mullvad.net/en/download/deb/2020.3/'
#    }

    doRemoteAlienDEB () {
        packageName='alien'
        remoteURL='http://archive.ubuntu.com/ubuntu/pool/universe/a/alien/alien_8.95_all.deb'
    }

    doRemoteOinkmasterDEB () {
        packageName='oinkmaster'
        remoteURL='http://archive.ubuntu.com/ubuntu/pool/universe/o/oinkmaster/oinkmaster_2.0-4_all.deb'
    }

    doRemoteOpenInvadersDEB () {
        packageName='open-invaders'
        remoteURL='http://ftp.debian.org/debian/pool/main/o/open-invaders/open-invaders_0.3-5_amd64.deb'
    }

    doRemoteRPMDEB () {
        packageName='rpm'
        remoteURL='http://archive.ubuntu.com/ubuntu/pool/main/r/rpm/rpm_4.12.0.1+dfsg1-3build3_amd64.deb'
    }

    # local targets
    doLocalRPMDEB () {
        packageName='rpm'
        localFilePath='~/Downloads/rpm_4.12.0.1+dfsg1-3build3_amd64.deb'
    }

    doLocalAlienDEB () {
        packageName='alien'
        localFilePath='~/Downloads/alien_8.95_all.deb'
    }

    doLocalOpenInvadersDEB () {
        packageName='open-invaders'
        localFilePath='~/Downloads/open-invaders_0.3-5_amd64.deb'
    }

#    doLocalFAudioXZ () {
#        packageName='faudio'
#        localFilePath='~/Downloads/faudio-19.01.tar.xz'
#    }

#    doLocalTorBrowserXZ () {
#        packageName='tor'
#        localFilePath='~/Downloads/tor-browser-linux64-9.0.4_en-US.tar.xz'
#    }

#    doLocalTorBrowserUpgrade () {
#        packageName='tor'
#        localFilePath='~/Downloads/tor-browser-linux64-9.0.5_en-US.tar.xz'
#    }

    # choose one, i.e only the last one will get handled
#    doRemoteOinkmasterDEB
#    doRemoteRPMDEB
#    doRemoteAlienDEB
    doRemoteOpenInvadersDEB
#    doLocalRPMDEB
#    doLocalAlienDEB
#    doLocalOpenInvadersDEB

#    targetDirectory='~/Downloads'
#    downloadOnly=true
#    harvestOnly=true
    return 0
}


# TODO - --prepare-install, run only download/extraction/fix-depends - same as build-dep
# TODO - --dry-run if possible. look into how to do that. maybe redirect installdir to /dev/null
# TODO - implement commandline arguments to quickset selections
parseArgs () {
    packageName=        # used to determine dependencies and name directories if necessary
    localFileName=      # can be determined later
    localFilePath=      # /path/to/file to be installed
    remoteURL=          # download target to be installed
    targetDirectory=
    sauceOnly=false     # if true, force install from source - useful for unpacking .debs and .rpms and such
    downloadOnly=false
    noConfirm=false     # if true, no confirmation prompts # done
    noDepends=false     # if true, ignore missing dependencies # done
    beVerbose=false     # if true, print all the output
    beQuiet=false       # if true, try not to print, print a lot
    quickStart=false    # if true, use quickDebugSelection instead of regular selection - for debug purposes

    # global system booleans, no commandline interactions
    isPackage=false
    isSource=false
    haslocalcopy=false
    remotetargetexists=false

    # unused and unrecognized args
    skippedArgs=()
    unrecognized=()

    for arg in "$@"; do
        case $arg in
            --name=* )
                packageName="${arg#*=}"
                shift
                ;;
            --from-url=* )
                if [ "$localFilePath" = "" ]; then
                    remoteURL="${arg#*=}"
                else
                    skippedArgs+=("${arg#*=}")
                fi
                shift
                ;;
            --from-file=* )
                if [ "$remoteURL" = "" ]; then
                    localFilePath="${arg#*=}"
                else
                    skippedArgs+=("${arg#*=}")
                fi
                shift
                ;;
            --to-directory=* )
                targetDirectory="${arg#*=}"
                shift
                ;;

            --from-source )
                sauceOnly=true
                shift
                ;;
            --download-only )
                downloadOnly=true
                shift
                ;;
            --yes-to-all )
                noConfirm=true
                shift
                ;;
            --ignore-depends )
                noDepends=true
                shift
                ;;
            --verbose )
                if ! $quiet; then
                    beVerbose=true
                else
                    skippedArgs+=("${arg#*=}")
                fi
                shift
                ;;
            --quiet )
                if ! $verbose; then
                    beQuiet=true
                else
                    skippedArgs+=("${arg#*=}")
                fi
                shift
                ;;
            --quickstart )
                quickStart=true
                shift
                ;;

            --pseudo-code )
                printNapkinStyleCode
                exit 0
                ;;
            --help )
                printHelp
                exit 0
                ;;
            --version )
                printVersion
                exit 0
                ;;
            * )
                unrecognized+=("${arg#*=}")
                shift
                ;;
        esac
    done
}

printNapkinStyleCode () {
    code="
# handleSelections()
#     if quickstart then
#         quickDebugSelection()
#             notifyuser details
#     elif localfilepath and packagename then
#         notifyuser details
#     elif remoteurl and packagename then
#         notifyuser details
#     else 
#         if packagename then set global
#         if localfilepath or remoteurl then set global
#         selectionMenu()                # recursion redirects here, in case anyone was wondering
#             promptuser details
#     validateFiletarget()
#         if error then
#             if promptuser restart then
#                 selectionMenu()        # recursion, hence the return code
#                 return 0
#             else exit 0
#         if not promptuser confirm then
#             if promptuser restart then
#                 selectionMenu()        # recursion, hence the return code
#                 return 0
#             exit 0
#         return 0
#     return 0
#
# handlePreparations()
#     if remoteurl then
#         if not haslocalcopy then        # file has NOT already been downloaded
#             promptuser confirmdownload
#             downloadFile()
#     localPreparations()
#         set localfilename
#         set localfilepath
#     validateDepends()
#         if unmetdepends then
#             promptuser details
#             dosomething?
#     if isarchive or sauceonly then
#         handleArchive()
#     return 0
#
# handleInstallation()
#     if ispackage then
#         installPackage()
#             promptuser confirminstall
#             if error then
#                 if promptuser trysauce then
#                     dpkg-unpack         # or equivalent for non-deb files
#                     installSource()
#                     return 0
#                 exit 0
#             postinstallstuff()
#             return 0                
#     if issauce then
#         installSource()
#             postinstallstuff()
#             return 0"
    echo "$code"; echo; echo "  job done"
}

# TODO - ensure all simple output strings are migrated to this function
# TODO - streamline userprompts (read anykey)
# prepared strings - prompts to the user
prepareFixedStrings () {
    # results and status
    result="Installation complete"                              # nice result
    badResult="Oops. Something went wrong"                      # prefix if installation fails
    permissionsFailed=", it appears to be a permission issue"   # permissions
    downloadError=" while downloading"                          # download
    verificationFailed=", local file could not be verified"     # local prep
    extractionError=" during extraction"                        # unpacking
    installationFailed=" during installation"                   # package installation
    AmbiguityExit=", cannot install both as package and source" # somewhere, something went wrong
    # general stuff # TODO - just redo all strings
    notifyGenericStopButton="For debugging purposes, press any key to continue"
    notifyScriptPurpose="This script will download and install whatever you tell it to"
    notifyInstallationFailed="Installation failed"
    notifyValidationPassed="Validation passed"
    # package selection
    promptForPackage="Enter the name of your desired package"
    promptForSource="Enter url or path to package. Leave blank if target in Downloads folder"
    notifySelectionSuccess="Selection complete"
    # validate target
    # validate permissions
    notifyDetectPrivilege="Validating privileges..."
    warnRootAccessDetected="root access detected, skipping further privilege tests"
    notifyInstallationTarget="Installation directory set to"
    notifyPermissionsSuccess="Permission check passed. Press any key to continue"
    # download
    notifyFileExists="File exists, skipping download"
    notifyDownloadSuccess="File succesfully downloaded"
    # local prep
    notifyLocalFilename="Local file name:"
    notifyLocalFilePath="local file path:"
    # depends
    notifyMissingDepends="The following dependencies are not installed:"
    # handleArchive
    notifyExtraction="Commencing extraction, this might take a while"
    notifyExtractionSuccess="Extraction succesful"
    notifyNoExtraction="Skipping extraction"
    # package installation
    # install from source
    warnStartConfigure="Preparations complete, commencing configuration"
    warnStartMake="Configuration complete, commencing make"
    warnStartMakeInstall="Finished make, commencing make install"
    unknownInstallationType="Unknown installation method, exiting"
}

printInstallationSucceeded () {
    notifyInstallationSucceeded="Succedfully installed $packageName"
    echo "$notifyInstallationSucceeded"
}

printPermissionsFailed () {
    notifyPermissionsFailed="
Full permissions are needed to install to this folder, current permissions are $1 and it should be 7xx.
You can change the permissions by using the following command:
"
    notifyPermissionsHelp="
sudo chmod 777 /usr/local/src/ for full access for anyone (not recommended)
sudo chmod 755 /usr/local/src/ for full access to you, and read+execute access for everyone else (recommended)
"
    echo; echo "$notifyPermissionsFailed"
    echo; echo "$notifyPermissionsHelp"
}

printConclusion () {
    if $haslocalcopy; then conclusion="local"
    else conclusion="remote"; fi
    notifyConclusion="Source determined to be $conclusion"
    echo "$notifyConclusion"
}

printPromptConclusionFailed () {
    promptForConclusion="Unable to determine source. Press (l) for local, or (r) for remote"
    userInput=
    echo -en "$promptForConclusion"
    while :; do
        read -sn 1 userInput
        if [ "$input" = 'l' ]; then haslocalcopy=true; break
        elif [ "$input" = 'r' ]; then haslocalcopy=false; break; fi
    done;
}

printPromptInstallRestartExit () {
    promptInstallRestartExit="Press (i) to install, (r) to restart, or (x) to exit"
    userInput=""
    if ! $noConfirm; then
        echo -en "$promptInstallRestartExit "
        while :; do            
            read -sn 1 userInput
            if [ "$userInput" = "x" ]; then gracefulExit; fi
            if [ "$userInput" = "i" ] || [ "$userInput" = "r" ]; then break; fi
        done; echo
    else userInput="i"; fi
}

printPromptIgnoreAutoExit () {
    promptIgnoreAutoExit="Press (i) to ignore, (a) to auto-install (experimental), or (x) to exit"
    userInput=""
    if ! $noConfirm; then
        echo -en "$promptIgnoreAutoExit "
        while :; do            
            read -sn 1 userInput
            if [ "$userInput" = "x" ]; then gracefulExit; fi
            if [ "$userInput" = "i" ]; return || [ "$userInput" = "a" ]; then break; fi
        done; echo
    else userInput="i"; fi
}

printPromptConfirmRemoteTarget () {
    promptConfirmRemoteTarget="Confirm downloading $localFileName to $targetDirectory?"
    notifyDownloadRestartExit="Press (i) to download, (r) to restart, or (x) to exit"
    userInput=""
    if ! $noConfirm; then
        echo "$promptConfirmRemoteTarget"
        echo -en "$notifyDownloadRestartExit "
        while :; do            
            read -sn 1 userInput
            if [ "$userInput" = "x" ]; then gracefulExit; fi
            if [ "$userInput" = "i" ] || [ "$userInput" = "r" ]; then break; fi
        done; echo
    else userInput="i"; fi
}

printPromptTrySource () {
    promptConfirmTrySource="Installation failed, but extraction might be possible"
    notifyExtractExit="Press (i) to attempt extraction and install from source, or (x) to exit"
    userInput=""
    if ! $noConfirm; then
        echo "$promptConfirmTrySource"
        echo -en "$notifyExtractExit "
        while :; do            
            read -sn 1 userInput
            if [ "$userInput" = "x" ]; then gracefulExit; fi
            if [ "$userInput" = "i" ]; then break; fi
        done; echo
    else userInput="i"; fi
}

printFileNotFound () {
    notifyFileNotFound="The file $localFilePath could not be verified"
    echo "  $notifyFileNotFound"
}

printInvalidURL () {
    notifyInvalidURL="Unable to establish connection: $remoteURL"
    echo "$notifyInvalidURL"
}

printVersion () {
    
    echo "$GTI2_NAME-$GTI2_VERSION"; echo
}

printHelp () {
    local helpUsage="
  Usage:

    gti2.sh [<option> ...]
"
    local helpDescription="
  Description:    

    TODO - $notifyScriptPurpose
"
    local helpOptions="
  Options:

    --name=[pkg_name]               Package name, used to determine dependencies and as subdirectory to
                                    installation directory if needed. Use the same name as apt/rpm/pacman.

    --from-url=[download link]      URL to http/ftp address where the package can be downloaded.

    --from-file=[/path/to/file]     If package is already downloaded, instruct gti2 to skip lookup/download
                                    process. If using on an offline device, use with --ignore-depends.
                                    TODO: functionality to get depends from local sources, e.g Downloads.

    --to-directory=[ins_dir]        Set installation directory to /path/to/dir/pkg_name/ or something like
                                    that. To be specific, if archive extraction is necessary, the [pkg_name]
                                    postfix will be ignored. If this option is not supplied, gti2 will set a
                                    default installation directory based on permissions. If this option is
                                    omitted, the default installation directories are:
                                        /usr/src/               if root privileges
                                        /usr/local/src/         if permitted
                                        $HOME/.local/share/     if no privileges
                                    Note: /usr/local/src/ is NOT allowed by default.

    --ignore-depends                Instruct gti2 to ignore dependencies. Be careful when using together
                                    with --yes-to-all, unless you want the experimental feature trying
                                    to install all kinds of sh!t.

    --from-source                   Instruct gti2 to install from source. If target is a package file,
                                    it will be extracted and follow normal source installation procedure.
                                    This option is NOT recursive and will NOT carry over into dependencies.

    --yes-to-all                    Skip all prompts, assuming yes to all. Exception is automatic
                                    installation of dependencies (see --ignore-depends).

    --verbose                       More output.
    --quiet                         Less output.

    --quickstart                    For debug purposes. This option will ignore all other commands and use
                                    hardcoded options. Requires the user to manually edit the script file,
                                    but can rapidly speed up the installation process.

    --pseudo-code                   Print pseudo code of this script, then exit.
    --help                          Print this help text, then exit.
    --version                       Print version information, then exit.
"
    local helpNotes="
  Notes:

    If --name is supplied, then either --from-url or --from-file must be supplied as well, and vice versa.
    If not, they will be ignored, and the interactive (?) menu will run.

    --from-url and --from-file are mutually exclusive.
    The script accepts any number of --from-url and --from-file options, but will ignore all but the first.

    If both --help and --pseudo-code are supplied, --help takes presedence.

    --quiet and --verbose can be supplied any number of times, but all except the first will be ignored.
"
    local helpExamples="
  Examples:

    gti2.sh
      Will launch an interactive (?) menu, prompting for pkg_name and either URL or PATH. If gti2 is unable
      to detect if it's URL or PATH, the user will be prompted.

    gti2.sh --verbose --quiet                     
      Like above, but will run in verbose mode. Verbose and quiet are mutually exclusive, and gti2 will
      use the first given instance of either, ignoring the instruction to be quiet. 

    gti2.sh --yes-to-all --to-directory=$HOME
      Will launch the interactive (?) menu, and upon confirmation of pkg_name and URL or PATH, proceed to
      install (without prompts) the selected program in $HOME/pkg_name/ as well as auto-install ALL
      dependencies in $HOME/dep_name/ 

    gti2.sh --from-source --yes-to-all --name=rpm --from-file=~Downloads/rpm_4.12.0.1+dfsg1-3build3_amd64.deb
      If the named rpm.deb is located in the downloads folder, gti2 will install all dependencies, then
      proceed to unpack the rpm.deb and run configure / make / make install. This process will never ask
      for confirmation.
"
    echo "$helpUsage"
    echo "$helpDescription"
    echo "$helpOptions"
    echo "$helpNotes"
    echo "$helpExamples"
}


# TODO - determine if root privileges and do proper installs if possible
# TODO - get and store system specs and capability for later use
#        example: no root privilege, .deb only package capability
# this is where $targetDirectory should be set if root-less, otherwise regular install
# permission needed to install to /usr/local/src
testPermissions () {
    local testdir="$1"
    access=($(stat -c "%a %n" "$testdir"))
    if [ $(expr substr "${access[0]}}" 2 2) != '77' ]; then return 1
    else return 0; fi
}

checkPrivilege () {
    if $(testPermissions "$targetDirectory"); then return 0
    else return 1; fi
}

testRoot () {
    if [ "$EUID" -eq 0 ]; then return 0
    else return 1; fi
}

# sets the installation directory prefix based on permissions by iterating a hardcoded list
dirtyTarget () {
    testarray=("/usr/src" "/usr/local/src" "$HOME/.local/share/")
    for dir in ${testarray[@]}; do
        targetDirectory="$dir"
        mkdir -p "$targetDirectory" > /dev/null 2>&1
        if checkPrivilege; then
            targetDirectory="$dir"
            break
        fi
    done
    if [ -d "$targetDirectory" ]; then return 0
    else return 1; fi
}

validatePermissions () {
    echo "$notifyDetectPrivilege"
    if testRoot; then
        echo "$warnRootAccessDetected"
        mkdir -p "$targetDirectory" > /dev/null 2>&1
        return 0
    fi
    if [ "$targetDirectory" = "" ] && dirtyTarget; then
        echo "$notifyInstallationTarget $targetDirectory"
    elif checkPrivilege && ! $noConfirm; then
        echo "$notifyPermissionsSuccess"
    else
        local tmpaccess=($(stat -c "%a %n" "$targetDirectory"))
        printPermissionsFailed "${tmpaccess[0]}" 
        return 1
    fi
    return 0
}

# TODO - remove all mentions of $sourceFile
handleSelections () {
    prepareFixedStrings
    if $quickStart; then
        quickDebugSelection
    elif [ "$packageName" = "" ]; then
        if [ "$localFilePath" != "" ] || [ "$remoteURL" != "" ]; then
            if selectionMenu; then return 0
            else return 1; fi
        fi
    fi
    local sourceFile=
    if [ "$localFilePath" != "" ]; then
        sourceFile="$localFilePath"
    elif [ "$remoteURL" != "" ]; then
        sourceFile="$remoteURL"
    else return 1; fi
#    cleanSelectionMenu
    if ! validateFileTarget "$sourceFile"; then return 1; fi
    if ! validatePermissions; then return 1; fi
    return 0
}

# menu function - lets the user select package, installation type and url/path
selectionMenu () {
#    cleanSelectionMenu
    local packName=
#    echo "$promptForPackage"
    read -p "$promptForPackage: " packName
#    cleanSelectionMenu "$packName"
    local sourceFile=
#    echo "$promptForSource"
    read -p "$promptForSource: " sourceFile

#    echo "package: $packName"
#    echo "source: $sourceFile"

    # TODO - ls /path/to/local/file and determine sourceFile - validateFileTarget?
    if [ "$sourceFile" = "" ]; then
        sourceFile="$HOME/Downloads/"
    fi

    # TODO - non-functional - fix
    if [ ! -f $sourceFile ]; then
        local listdir=()
        local tmplist+=$(ls "$sourceFile" | grep -iZ "$packName")
#        echo "found ${#listdir[@]} possible targets, listing:"
        for entry in ${tmplist[@]}; do
#        for entry in $(ls "$sourceFile" | grep -iZ "$packName"); do
            listdir+="$entry"
#            echo "$entry"
#            echo "iteration works!"
        done

        # TODO - if only one match, prompt confirm as target
#        if [ ${#possibleTargets} -eq 1 ]; then
#            doPromptConfirm
#        fi
        echo "found ${#listdir[@]} possible targets, listing:"
        for entry in ${listdir[@]}; do
#            listdir+="$entry"
            echo "$entry"
            echo "iteration works!"
        done
#        echo "$listdir" # | grep -i "$packName" # ignore-case
        if ! $noConfirm; then
            echo "$notifyGenericStopButton"
            read -sn 1 anykey
        fi
    fi

    if [ ! validateFileTarget "$sourceFile" ]; then
        if $haslocalcopy; then printFileNotFound
        else printInvalidURL; fi
        gracefulRestart # recursive call to self
    fi
    if ! validatePermissions; then
        echo # TODO - move permission-related error message here
        packName=""; sourceFile=""; isPackage=false; isSource=false
        gracefulRestart # recursive call to self
    fi
    
    # confirmation before installing selection
    printPromptInstallRestartExit; echo
    if [ "$userInput" = "r" ]; then gracefulRestart
    else echo "- proceeding!"; fi
    # response to user input - y to continue with selected parameters, n to start over
    packageName="$packName"
    if ! $haslocalcopy; then remoteURL="$sourceFile"
    elif $haslocalcopy; then localFilePath="$sourceFile"; fi
    return 0
}

# TODO - offer to keep existing choices
gracefulRestart () {
    echo "- restarting!"
    packageName=
    localFileName=
    localFilePath=
    remoteURL=
    targetDirectory=
    isPackage=false
    isSource=false
    haslocalcopy=false
    remotetargetexists=false
    fixSelectionMenu
    selectionMenu
    return $?
}

gracefulExit () {
    echo "$result"
    exit 0
}

fixSelectionMenu () {
    # line-editing the shell
    # TODO - echo -n "no auto newline" echo -e "can do crazy stuff e.g \e1A moves pointer 1 line up"
    echo
    return 0
}

validateFileTarget () {
    # fix tilde-style path to $HOME subdirs for targetdirectory
    local length=${#targetDirectory}
    if [ "$targetDirectory" != "" ] && [ ${targetDirectory:0:1} = "~" ]; then
        targetDirectory="$HOME${targetDirectory:1:$length-1}"
    fi
    # test if local file or download
    local sourceFile="$1"
    length=${#sourceFile}
    # local - based in home dir - convert to true path
    if [ ${sourceFile:0:1} = "~" ]; then sourceFile="$HOME${sourceFile:1:$length-1}"; fi
    # local filepath - skip download
    if [ ${sourceFile:0:1} = "/" ]; then haslocalcopy=true
    # remote - download needed
    elif [ ${sourceFile:0:4} = 'http' ] || [ ${sourceFile:0:3} = 'ftp' ]; then haslocalcopy=false
    # cannot be determined, ask user for local or remote
    else printPromptConclusionFailed; echo; fi
    if ! $beQuiet; then printConclusion; fi
    if $haslocalcopy; then
        # test if file exists
        if [ -f "$sourceFile" ]; then
            localFilePath="$sourceFile"
        else return 1; fi # message to user at source
    else
        # test if remote url is valid
        if $beVerbose; then wget --spider $sourceFile
        elif $beQuiet; then wget -q --spider $sourceFile
        else wget -nv --spider $sourceFile; fi
        if [ $? -eq 0 ]; then
            remotetargetexists=true
            remoteURL="$sourceFile"
        else return 1; fi
    fi
    return 0
}

handlePreparations () {
    # if remote file - commence downloading
    if ! $haslocalcopy && $remotetargetexists; then
        if ! downloadFile; then return 1; fi
    fi
    if $downloadOnly; then return 0; fi
    if $haslocalcopy; then
        if ! localPreparations; then return 1; fi
    fi
    if ! validateDepends; then return 1; fi
    if ! handleArchive; then return 1; fi
    return 0
}

# TODO - rename
# self-explanatory - migrated to reduce bloating
downloadFile () {
    local urlprefix= # TODO - check if scrappable
    local i=0
    local length=${#remoteURL}
    # splitting filename from url prefix
    while [ $i -lt $length ]; do
        char=${remoteURL:$i:1}
        if [ $char = "/" ]; then
            urlprefix=${remoteURL:0:($i+1)} # TODO - check if scrappable
            localFileName=${remoteURL:($i+1):$length}
        fi
        i=$(expr $i + 1)
    done
    # fix local file path, depending on path provided
    if [ ${targetDirectory: -1} = '/' ]; then
        localFilePath="$targetDirectory$localFileName"
    else localFilePath="$targetDirectory/$localFileName"
    fi
    # test if file exists - no need to download something we already have
    if [ -f "$localFilePath" ]; then
        haslocalcopy=true
        echo $notifyFileExists
        return 0
    fi
    printPromptConfirmRemoteTarget
    if [ ! "$userInput"  = "i" ]; then gracefulRestart
    else echo "- proceeding!"; fi
    # no-verbose, no-clobber, --no-check-certificate, --show-progress, --directory-prefix
    if $beVerbose; then
        wget -nc --no-check-certificate --show-progress "$remoteURL" -P "$targetDirectory"
    elif $beQuiet; then
        wget -q -nc --no-check-certificate --show-progress "$remoteURL" -P "$targetDirectory"
    else
        wget -nv -nc --no-check-certificate --show-progress "$remoteURL" -P "$targetDirectory"
    fi
    # test if file was actually downloaded
    dlSuccess=$?
    if [ $dlSuccess -eq 0 ] && ! $beQuiet; then
        echo "$notifyDownloadSuccess"
        haslocalcopy=true
        return 0
    fi
    result="$badResult$downloadError"
    return 1
}

# remaining local file operations before installing - migrated to reduce bloating
# TODO - autocomplete incomplete package names - e.g correct 'Mullvad' to 'MullvadVPN-2020.3_amd64.deb'
# TODO - autocorrect poor spelling - e.g correct 'MulvadVPN-2020.3_x86_64.rpm' to MullvadVPN-2020.3_x86_64.rpm
# TODO - rename
localPreparations () {
    local length=${#localFilePath}
    local pathtofile=
    local i=0
    # split actual filename from the rest, for reasons...
    while [ $i -lt $length ]; do
        local char=${localFilePath:$i:1}
        if [ $char = "/" ]; then
            pathtofile=${localFilePath:0:($i+1)}
            localFileName=${localFilePath:($i+1):$length}
        fi
        i=$(expr $i + 1)
    done
    if ! $beQuiet; then
        echo "$notifyLocalFilename $localFileName"
        echo "$notifyLocalFilePath $localFilePath"
    fi
    # check if [package_name] is present in the target directory
    if [ ! -f $localFilePath ]; then
        printFileNotFound
        echo "$installationAborted"
        result="$badResult$verificationFailed"
        return 1
    fi
    # analyze file type
    fileData=$(file -b "$localFilePath")
    if $beVerbose; then
        echo "$fileData"
    fi
    if echo "$fileData" | grep Debian; then
        isPackage=true
        fileType='deb'
    elif echo "$fileData" | grep RPM; then
        isPackage=true
        fileType='rpm'
    elif echo "$fileData" | grep compressed; then
        isArchive=true
        fileType='tar'
    elif echo "$fileData" | grep archive; then
        isArchive=true
        fileType='zip'
#    elif echo "$fileData" | grep ...; then
    fi    
    return 0
}

# TODO - temporary file handling - what and when to delete
# TODO - depends info - make sure it works for all file types incl source
# TODO - feature - auto-install dependencies
# determine if selected package has dependencies
validateDepends () {
    if $noDepends; then return 0; fi
    # create tmp dir+files for output storage
    mkdir -p /tmp/gti2/
    local tmpstdout='/tmp/gti2/gti2std.out'
    local tmpstderr='/tmp/gti2/gti2std.err'
    touch "$tmpstdout" "$tmpstderr"
    # extract depends info from target package
    # TODO - depends info - make sure it works for all file types incl source
    local depends=
    local allDepends=()
    # get dpkg-info and pipe output to file - herefiles might be better
    dpkg --info "$localFilePath" 2>$tmpstderr 1>$tmpstdout # TODO - verify redirect to varpaths
    # isolate the depends section
    depends=$(cat $tmpstdout | grep -FZ Depends)
    # parse depends string into 1 space-separated list without <title>Depends
    # note: if minimum required version, it fucks up everything
    depends=($(echo ${depends//\: / } | tr -d " " | cut -c 8-))
    # split depends string into array
    readarray -td, allDepends <<<"$depends"
    # delete temporary files
    rm -R /tmp/gti2/
    local i=0
    missingDepends=()
    for dep in ${allDepends[@]}; do
        if [ ! $(expr index "$dep" '\(') = 0 ]; then
            local end=$(expr index "$dep" '\(')
            local dep=${dep:0:$end-1}
        fi
        if ! $(dpkg -s "$dep" &> /dev/null); then
            missingDepends+=("$dep")
            i=$(expr $i + 1)
        fi
    done
    # list depends
    local miss=${#missingDepends[@]}
    if [ $miss -ne 0 ]; then
        echo; echo "$notifyMissingDepends"
        for dep in ${missingDepends[@]}; do
            echo "  $dep"
        done
        printPromptIgnoreAutoExit
        # response to user input - i to ignore missing depends, a to try auto-install
        if [ "$userInput" = "i" ]; then
            echo "- ignoring!"
            return 0
        elif [ "$userInput" = "a" ]; then
            if notReallyExperimentalFeature; then return 0
            else return 1; fi
        fi
    fi
}

dirtySource () {
    depdata=$(apt-get --print-uris download "$1" | awk '{print $1}')
    remoteURL=$(echo "$depdata" | tr -d \')
    return 0
}

usingInfoFromAPT () {
    if ! handleSelections; then return 1; fi
    echo "selections done"
    if ! handlePreparations; then return 1; fi
    echo "preparations done"
    if ! handleInstallation; then return 1; fi
    echo "Succesfully installed $packageName"
    return 0
}

notReallyExperimentalFeature () {
    local errorsEncountered=false
    failedToInstall=()
    # using apt
    command -v apt &> /dev/null
    if [ $? -eq 0 ]; then
        echo "- APT found!"
        # do stuff
#        depList=() # only for data-harvesting purposes
        setCurrentPackage
        for dep in ${missingDepends[@]}; do
            echo "$dep"
            setDependPackage "$dep"
            dirtySource "$dep"
            dirtyTarget
            echo "$remoteURL"
            echo "install dir: $targetDirectory"
            if ! usingInfoFromAPT; then
                errorsEncountered=true
                failedToInstall+="$dep"
            fi
        done
        if ! $errorsEncountered; then
            activateCurrentPackage
            return 0
        else echo "errors encountered, the following failed to install:"; fi
        for dep in ${failedToInstall}; do
            echo "  $dep"
        done
    fi
    
    echo "- apt not found?"
    return 0
    # rpm? yum? I have no clue how it works
    command -v rpm &> /dev/null
    if $?; then
        echo "RPM found!"
        # do stuff
        return 0
    fi

    # rpm? yum? I have no clue how it works
    command -v pacman &> /dev/null
    if $?; then
        echo "pacman found, let's see what happens when live"
        # do stuff
        return 0
    fi

    # back to plan A - guessing uri's and stuff
    if superExperimentalFeature; then return 0
    else return 1; fi
}

setCurrentPackage () {
    currentPackageName="$packageName"
    currentPackageIsPackage=$isPackage
    currentPackageIsSource=$isSource
    currentPackageLocalFileName="$localFileName"
    currentPackageLocalFilePath="$localFilePath"
    currentPackageRemoteURL="$remoteURL"
    currentPackageTargetDirectory="$targetDirectory"
    currentPackageSauceOnly="$sauceOnly"
    currentPackageNoConfirm="$noConfirm"
    currentPackageNoDepends="$noDepends"
    currentPackageBeVerbose="$beVerbose"
    currentPackageBeQuiet="$beQuiet"
    currentPackageHasLocalCopy="$haslocalcopy"
    currentPackageRemoteTargetExists="$remotetargetexists"
}

setDependPackage () {
    packageName="$1"
    localFileName=
    localFilePath=
    remoteURL=
    targetDirectory=
    sauceOnly=false
    downloadOnly=false
    NoConfirm=true
    NoDepends=true
    beVerbose=false
    beQuiet=false
}

activateCurrentPackage () {
    packageName="$currentPackageName"
    isPackage=$currentPackageIsPackage
    isSource=$currentPackageIsSource
    localFileName="$currentPackageLocalFileName"
    localFilePath="$currentPackageLocalFilePath"
    remoteURL="$currentPackageRemoteURL"
    targetDirectory="$currentPackageTargetDirectory"
    sauceOnly="$currentPackageSauceOnly"
    noConfirm="$currentPackageNoConfirm"
    noDepends="$currentPackageNoDepends"
    beVerbose="$currentPackageBeVerbose"
    beQuiet="$currentPackageBeQuiet"
    haslocalcopy="$currentPackageHasLocalCopy"
    remotetargetexists="$currentPackageRemoteTargetExists"
}


# TODO - flip the comments to restore original functionality
# TODO - intended to manage installation of missing depends
# TODO - the commented-out bits are the original pieces, what makes the things work
# TODO - depList=() and goHarvest() are data-harvesters only, although depList=() might be useful
# TODO - update and validate return values
# this super experimental feature auto-installs missing dependencies
superExperimentalFeature () {
    if ! getDistro; then return 1; fi
    depList=() # only for data-harvesting purposes
#    setCurrentPackage
    for dep in ${missingDepends[@]}; do
#        setDependPackage "$dep"
#        installMissing
        goHarvest "$dep" # only for data-harvesting purposes
    done
#    echo "finished writing"
    for dep in ${depList[@]}; do
        echo $dep
    done
    if $harvestOnly; then exit 0; fi
#    activateCurrentPackage
    return 0
}

# TODO - works - but cheating - keep pondering
# TODO - harvest info via apt - the commented-out bits are for writing to file
# TODO - the file is intended for gathering data, to optimize the URL-building algorithm
# unused ??
goHarvest () {
    local dep="$1"
    depList+=($dep)
    zsection=$(apt show $dep | grep -Z Section)
    depList+=($(echo ${zsection//\: / } | tr -d " " | cut -c 8-))
    zversion=$(apt show $dep | grep -Z Version)
    depList+=($(echo ${zversion//\: / } | tr -d " " | cut -c 8-))
    if $harvestOnly; then
        local target="$HOME/harvested_details.txt"
        # TODO - ignore if data exists in file
        echo "$dep" >> "$target"
        for derp in ${depList[@]}; do
            echo $derp >> "$target"
        done
        echo >> "$target"
    fi
}

# unused - for now
getDistro () {
    unamecommand=$(uname -v)
    if echo "$unamecommand" | grep Ubuntu; then
        distro="Ubuntu"
    elif echo "$unamecommand" | grep Debian; then
        distro="Debian"
#    elif echo "$unamecommand" | grep ...; then
    else
        notifyDistroNotDetected="Distro $distro not supported, exiting"
        echo "$notifyDistroNotDetected"
        return 1
    fi
    return 0
}

# TODO - experimental feature - unused for now
# install missing depends
installMissing () {
    if ! handleSelections; then return 1; fi
    if ! handlePreparations; then return 1; fi
    if ! handleInstallation; then return 1; fi
    echo "Succesfully installed $packageName"
    return 0
}

# unused - for now
determineURL () {
    case $distro in
        Ubuntu | Debian ) # TODO - add more debian-based options
            repo=$(getDebRepo)
            ;;
        something_else )
            repo=$(getRepository "rpm")
            ;;
    esac
    echo "$repo"
}

# unused - for now
getDebRepo () {
    while read -r line; do
        if [ "${line:0:3}" != "$1" ]; then
            continue
        else
            ftpserver=($line)
            break
        fi
    done < /etc/apt/sources.list
    # dk.archive.ubuntu.com/ubuntu/
    baseserver=${ftpserver[1]} # pulls the proper mirror from the APT sources.list # requires APT
    # group name - e.g pool/
    # subgroup name - e.g main/
    # subsubgroup name - e.g r/
    # $dependName - e.g rpm
    # remote file name - absolutely fucked up - e.g rpm_4.12.0.1+dfsg1-3build3_amd64.deb
    # cheat - if we use sources.list from APT, why not use apt show for determining the remote target
    depPrefix=${dependName:0:1} # subsubgroup - not cheating, educated guess, might be wrong
    echo "work in progress" # NOTE: this is the returned "object" hence no return code
}

    # TODO - maybe later - bedtime
    # wget "$ftpserver" --recursive --no-parent --spider --directory-prefix="$GTI2_INST_DIR"
    # add --quiet and --show-progress # note: --level=int (depth) might be useful? --relative --accept $packageName
    # takes too long, and takes up a lot of space
    # maybe iterative approach, guesstimating where it's located
    # another option could be to supply a tree structure with the script,
    # and only check for dirs not in the tree, updating the tree over time
    # possibly a combination of these if nothing else shows up
    # example: for dep open-invaders-data
    # ubuntu: wget pool/main/o/ pool/universe/o/ pool/restricted/o/ and pool/multiverse/o/
    # debian: wget pool/contrib/o/ pool/main/o/ and pool/non-free/o/


# TODO - output control - don't echo 3k lines of files, but echo something
# TODO - simplification - let's just test the compression and support only specific formats
# TODO - functionality  - support more formats
# unpack archives with source files
handleArchive () {
    if $isPackage && ! $sauceOnly; then
        if ! $beQuiet; then echo "$notifyNoExtraction"; fi
        return 0
    elif $isPackage && $sauceOnly; then
        if ! dpkgExtration; then return 1; fi
    elif $isSource; then
        if ! archiveExtraction; then return 1; fi
#    elif ...; then # other types?
    fi
    return 0
}

# TODO - confirm process works
# TODO - migrate dpkg extration process here
dpkgExtraction () {
    dpkg -x "$localFilePath" "$targetDirectory"
    if [ $? = 0 ]; then
        isSource=true
        isPackage=false
        return 0
    else return 1; fi
}

archiveExtraction () {
    # determine compression type
    compressionType=$(file -b "$localFilePath" | cut -d ' ' -f 1)
    # logfile needed for installation
    # could probably do a workaround for this and pipe output to /dev/null instead, or use a here-file
    logfile="$localFilePath.LOG"
    touch "$logfile"
    local extractionSuccess=false
    echo "$notifyExtraction"
    # switch, lots of different archive formats, and potential for undiscovered cases
#    case "$compressionType" in
    case "$fileType" in
        zip ) # probably more formats to catch
            unzip -an "$localFilePath" -d "$targetDirectory"
            extractionSuccess=true
            ;;
        tar )
            tar -xvf "$localFilePath" -C "$targetDirectory" --skip-old-files --force-local --warning=none > "$logfile" 2>&1
            extractionSuccess=true
            ;;
    esac
    if $extractionSuccess; then
        echo "$notifyExtractionSuccess"
        return 0
    else
        result="$badResult$extractionError"
        return 1
    fi
}

# TODO - reflection - this is too simple, is it even needed?
handleInstallation () {
    if $downloadOnly; then return 0
    elif $isSource && ! $isPackage; then
        if ! installSource; then return 1; fi
    elif $isPackage && ! $isSource; then
        if ! installPackage; then return 1; fi
    else
        if ! $beQuiet; then echo "$notifyAmbiguityExit"; fi # remove echo and append reason to $result
        return 1
    fi
    return 0
}


# TODO - optimize for .deb files using dpkg
#        suggestions:   install alien (convert to .deb) or rpm if on debian-based system
#                       no idea about the reverse situation (e.g use deb-files on fedora)
# TODO - implement solutions for non-deb files
#        gentoo (portage)                       eBuild/emerge: (always source)
#        redhat/fedora/centos/suse/mandriva     .rpm
#        arch   pacman (binaries only)
# non-deb list: flatpak, petget, entropy, nix, 
# handling and installation of package files .rpm or .deb
# for now, focus on dpkg / .deb files, since .rpm files cannot be easily tested without installing stuff
# also, this script is supposed to work on any machine with any distro using any package manager
installPackage () {
    packageSize=${#localFileName}
#    echo $(expr substr $localFileName $((packageSize-2)) 3)
    if ! $noConfirm; then
        echo "$warnInstallationStart"
        read -sn 1 anykey
    fi
    # .RPM files
    # TODO - use file command to determine filetype
    cd "$targetDirectory"
    if [ $(expr substr $localFileName $((packageSize-2)) 3) == 'rpm' ]
    then 
        echo this is an RPM file.
        #alien -k $packageName
#        i=$(alien -k $localFileName)
        echo "rpm"
        i=0
        while :; do
            echo "."
            wait 1
            i=$(expr $i + 1)
        done
        itemp=( $i )
        echo "remove"
#        rm $localFileName
        localFileName=${itemp[0]}
        echo "RPM process for $localFilePath complete"
        return 0
    fi
   # .DEB files
#    isPackage=true
#    fileType='rpm'
    if [ "$filetype" = 'deb' ]; then
        if $beQuiet; then
            dpkg --force-not-root instdir="$targetDirectory" --install "$localFilePath" &> /dev/null
            if [ $? -eq 0 ]; then printInstallationSucceeded; fi
        else dpkg --force-not-root instdir="$targetDirectory" --install "$localFilePath"; fi
        if [ $? -ne 0 ]; then
            echo "$notifyInstallationFailed"
            printPromptTrySource
            echo "- outsourcing!"
            dpkgExtraction
            isPackage=false
            isSource=true
            installSource   # TODO verify if at all doable
            return $?
        fi
        return 0
    fi
}


# TODO - fallback if this is not the case, as it will break everything atm
# TODO - fallback method if no configure
# TODO - proper export path - verify actual dirs needed
# installation from source
installSource () {
    if ! $noConfirm; then
        echo "$warnInstallationStart"
        read -sn 1 anykey
    fi
    # the very first line in the archive is *usually* a directory containing everything else
    local installDir=$(head -n 1 "$logfile")
    if [ ! -d installDir ]; then return 1 # TODO - implement fallback / alternative scenarios
    else cd "$targetDirectory$installDir"; fi
    # testing for ./configure
    local progress=
    if ( ls configur* > /dev/null ); then
        # throw all the data at the user - not perfect
        if $beQuiet; then
            # TODO - verify that the return code is NOT from tee command or append to log
            if ! $noConfirm; then promptContinue "$warnStartConfigure"; fi
            ./configure --prefix="$targetDirectory" | tee >> "$logfile"
            progress=$?; if ! $progress; then return 1; fi # abort installation if unsuccesful
            if ! $noConfirm; then promptContinue "$warnStartMake"; fi
            make | tee >> "$logfile"
            progress=$?; if ! $progress; then return 1; fi # abort installation if unsuccesful
            if ! $noConfirm; then promptContinue "$warnStartMakeInstall"; fi
            make install | tee >> "$logfile"
            progress=$?; if ! $progress; then return 1; fi # abort installation if unsuccesful
        else
            if ! $noConfirm; then promptContinue "$warnStartConfigure"; fi
            ./configure --prefix="$targetDirectory"
            progress=$?; if ! $progress; then return 1; fi # abort installation if unsuccesful
            if ! $noConfirm; then promptContinue "$warnStartMake"; fi
            make
            progress=$?; if ! $progress; then return 1; fi # abort installation if unsuccesful
            if ! $noConfirm; then promptContinue "$warnStartMakeInstall"; fi
            make install
            progress=$?; if ! $progress; then return 1; fi # abort installation if unsuccesful
        fi
        return 0
#    elif ...; then # TODO - fallback method if no configure
    else 
        result="$badResult$unknownInstallationType"
        return 1
    fi
}

# TODO - if root access, add to proper path env
# TODO - proper exporting of needed path
# sort of fix for lack of admin privileges
postInstallOperations () {
    export PATH="$targetDirectory'bin':$PATH"
    # TODO - cleanup
}

# TODO - might as well be an option
# might be useful
sourceFromGit () {
    downloadlink=$1
    targetdir=$2
    packageName=$3
    cd $targetdir
    workingDirectory=$(pwd)
    echo "working directory: $workingDirectory"
    echo "install $packageName to $workingDirectory (y/n)?"
    read conf
    if [ "$conf" = y ]
    then
        git clone $downloadlink
    fi
}

runMain () {
    if ! handleSelections; then return 1; fi
    if ! handlePreparations; then return 1; fi
    if ! handleInstallation; then return 1; fi
    return 0
}

umask 0 # validate if always needed, move to where needed, unset when no longer needed
parseArgs "${@}"
if runMain; then
    echo "$result"
    exit 0
fi
echo "$result"
exit 1
}
